package rvenancio2.br.com.economize_app.activity;

import android.app.Fragment;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.MultiAutoCompleteTextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;

import rvenancio2.br.com.economize_app.R;
import rvenancio2.br.com.economize_app.fragment.MapaFragment;

//import com.google.maps.model.LatLng;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {

    private MapaFragment mapaFragment;
    private Properties extras;
    private ArrayAdapter<String> adMultiPesquisa;
    private MultiAutoCompleteTextView textViewPesquisa;
    private Button btPesquisa;
    private static String[] CITY;
    public LatLng ondeIniciaMapa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        try{

            extras = getProperties("config.properties", this);
            extras = null;

            if(extras != null){
                String temp =  extras.getProperty("ondeIniciaMapa");
                StringTokenizer s = new StringTokenizer(temp, ",");

                ArrayList<Double> array = new ArrayList<Double>();

                while (s.hasMoreElements()) {
                    array.add(Double.parseDouble((String) s.nextElement()));
                }

                if (array.size() != 2 || array.isEmpty()) {
                    System.out.println("erro!");
                } else {
                    ondeIniciaMapa = new LatLng(array.get(0), array.get(1));
                }
            }

            initComponets();


           // FragmentTransaction fragmentTransaction;
            //fragmentTransaction = getFragmentManager().beginTransaction();
            //fragmentTransaction.add(R.id.map1, getMapaFragment());
           // fragmentTransaction.commit();
            //mapaFragment.getMapAsync(this);

            registerEvents();

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void registerEvents() {

        textViewPesquisa.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                textViewPesquisa.setText("");
            }});

        textViewPesquisa.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                textViewPesquisa.setText("");
                return false;
            }
        });



        btPesquisa.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


            }
        });

    }


    private void initComponets() {

        setMapaFragment(new MapaFragment());

        btPesquisa = (Button) findViewById(R.id.buttonPesquisar);

        adMultiPesquisa = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, CITY);
        //???
        adMultiPesquisa.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1);
        textViewPesquisa = (MultiAutoCompleteTextView) findViewById(R.id.multiAutoCompletePesquisaUsuario);
        //
        //textViewOrigem.addTextChangedListener(filterTextWatcher);
        textViewPesquisa.setThreshold(1);
        textViewPesquisa.setAdapter(adMultiPesquisa);
        //
        textViewPesquisa.setTokenizer(new SpaceTokenizer());
        //textViewOrigem.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int clickedItem = item.getItemId();

        switch (clickedItem) {
            case R.id.action_settings:
                // Intent irParaFormulario = new Intent(this, Formulario.class);
                // startActivity(irParaFormulario);
                break;

            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    public MapaFragment getMapaFragment() {
        return mapaFragment;
    }

    public void setMapaFragment(MapaFragment mapa) {
        this.mapaFragment = mapa;
    }

    @Override
    public void onMapReady(GoogleMap map) {

        getMapaFragment().setMap(map);
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        getMapaFragment().getMapa().addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        getMapaFragment().getMapa().moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    public static Properties getProperties(String file, Context context) throws IOException {
        Properties properties = new Properties();;
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = assetManager.open(file);
        properties.load(inputStream);
        return properties;

    }



    public class SpaceTokenizer implements MultiAutoCompleteTextView.Tokenizer {

        public int findTokenStart(CharSequence text, int cursor) {
            int i = cursor;

            while (i > 0 && text.charAt(i - 1) != ' ') {
                i--;
            }
            while (i < cursor && text.charAt(i) == ' ') {
                i++;
            }

            return i;
        }

        public int findTokenEnd(CharSequence text, int cursor) {
            int i = cursor;
            int len = text.length();

            while (i < len) {
                if (text.charAt(i) == ' ') {
                    return i;
                } else {
                    i++;
                }
            }

            return len;
        }

        public CharSequence terminateToken(CharSequence text) {
            int i = text.length();

            while (i > 0 && text.charAt(i - 1) == ' ') {
                i--;
            }

            if (i > 0 && text.charAt(i - 1) == ' ') {
                return text;
            } else {
                if (text instanceof Spanned) {
                    SpannableString sp = new SpannableString(text + "");
                    TextUtils.copySpansFrom((Spanned) text, 0, text.length(),
                            Object.class, sp, 0);
                    return sp;
                } else {
                    return text + "";
                }
            }
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }



    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Fragment fragment;

        if (id == R.id.nav_camera) {
            //fragment = new YourFragment();
            //FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            //ft.replace(R.id.mainFrame, fragment);
            //ft.commit();
        }
        else if (id == R.id.nav_gallery) {
            //Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            //startActivity(intent);
        }
        else if (id == R.id.nav_slideshow) {

        }
        else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
