package rvenancio2.br.com.economize_app.fragment;


import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;


public class MapaFragment extends MapFragment {

	private GoogleMap googleMap;


	public MapaFragment(){
	}


	@Override
	public void onResume() {
		super.onResume();
		//FragmentActivity context = getActivity();
		googleMap = getMap();
	}

	public void centralizaNo(LatLng local) {
		// GoogleMap map = getMap();
		CameraUpdate update = CameraUpdateFactory.newLatLngZoom(local, 15);
		this.googleMap.animateCamera(update);
	}


	public GoogleMap getMapa() {
		return googleMap;
	}

	public void setMap(GoogleMap map){
		this.googleMap = map;
	}
}
