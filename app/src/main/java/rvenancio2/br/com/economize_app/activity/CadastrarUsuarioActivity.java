package rvenancio2.br.com.economize_app.activity;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import rvenancio2.br.com.economize_app.R;
import rvenancio2.br.com.economize_app.sqlite.UsuarioDAO;
import rvenancio2.br.com.economize_app.sqlite.UsuarioLite;
import rvenancio2.br.com.economize_app.task.CadastraUsuarioAsyncTask;

/**
 * Created by rvena on 04/10/2017.
 */


public class CadastrarUsuarioActivity extends Activity {

    public Properties properties;
    public String ipServidor;
    public String pathServicesServidor;
    CadastrarUsuarioActivity activity;
    EditText nome;// = (EditText)findViewById(R.id.editTextNome);
    EditText cpf;// = (EditText)findViewById((R.id.editTextCPF));
    EditText email;// = (EditText)findViewById(R.id.editTextEmail);
    EditText senha1;// = (EditText)findViewById(R.id.editTextSenha1);
    EditText senha2;// = (EditText)findViewById(R.id.editTextSenha2);
    EditText dataNasc;// = (EditText)findViewById(R.id.editTextDataNasc);
    Button botao;// = (Button)findViewById(R.id.button);


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.cadastrar_usuario_layout);
        nome = (EditText)findViewById(R.id.editTextNome);
        cpf = (EditText)findViewById((R.id.editTextCPF));
        email = (EditText)findViewById(R.id.editTextEmail);
        senha1 = (EditText)findViewById(R.id.editTextSenha1);
        senha2 = (EditText)findViewById(R.id.editTextSenha2);
        dataNasc = (EditText)findViewById(R.id.editTextDataNasc);
        botao = (Button)findViewById(R.id.button);

        try {
            properties = getProperties("config.properties", this);
        }catch (Exception e){
            e.printStackTrace();
        }

        if(properties==null){
            Toast.makeText(this, "Erro ao ler properties.", Toast.LENGTH_SHORT).show();
            finish();
        }else {
            //iv.startAnimation(an);
            ipServidor = properties.getProperty("ipServidor");
            pathServicesServidor = properties.getProperty("pathServicesServidor");
            //broadcastReceiver = new WifiReceiver(this);
            //intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
            //registerReceiver(broadcastReceiver, intentFilter);
        }




        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //BancoController crud = new BancoController(getBaseContext());
                //UsuarioDAO dao = new UsuarioDAO(getBaseContext());
                //Usuario all =  dao.recuperarTodos().get(0);
                String teste = null;
                String nomeString = nome.getText().toString();
                String cpfString = cpf.getText().toString();
                String emailString = email.getText().toString();
                String senha1String = senha1.getText().toString();
                String senha2String = senha2.getText().toString();
                //String resultado;

                if (!senha1String.equals(senha2String)){
                    Toast.makeText(getApplicationContext(), "SENHAS NAO CONFEREM", Toast.LENGTH_LONG).show();
                }else{
                    try {
                        teste = new CadastraUsuarioAsyncTask(activity).execute(ipServidor, pathServicesServidor,
                                nomeString,cpfString,emailString,senha1String).get();
                        UsuarioLite u = new UsuarioLite();
                        Gson gson = new Gson();
                        UsuarioLite obj = gson.fromJson(teste, UsuarioLite.class);

                        UsuarioDAO dao = new UsuarioDAO(getBaseContext());
                        dao.salvar(obj);
                        //TODO MANDAR PARA ACTIVITY PRINCIPAL

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
                //resultado = crud.insereDado(tituloString,autorString,editoraString);
                //Toast.makeText(getApplicationContext(), resultado, Toast.LENGTH_LONG).show();


            }
        });
        activity = this;

    }


    public static Properties getProperties(String file, Context context) throws IOException {
        Properties properties = new Properties();;
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = assetManager.open(file);
        properties.load(inputStream);
        return properties;
    }

}