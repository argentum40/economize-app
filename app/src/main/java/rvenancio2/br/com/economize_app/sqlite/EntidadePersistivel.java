package rvenancio2.br.com.economize_app.sqlite;

/**
 * Created by rvena on 05/10/2017.
 */

public interface EntidadePersistivel {

    public Integer getId();
    public void setId(Integer id);

}
