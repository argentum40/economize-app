package rvenancio2.br.com.economize_app.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import rvenancio2.br.com.economize_app.R;
import rvenancio2.br.com.economize_app.model.Usuario;
import rvenancio2.br.com.economize_app.sqlite.UsuarioDAO;
import rvenancio2.br.com.economize_app.sqlite.UsuarioLite;
import rvenancio2.br.com.economize_app.task.LoginAsyncTask;

/**
 * Created by rvena on 04/10/2017.
 */


public class SplashActivity extends Activity {

    public Properties properties;
    public String ipServidor;
    public String pathServicesServidor;
    //public String portaServidorGeoserver;
    //IntentFilter intentFilter = new IntentFilter();
    public boolean flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_splash_economize);
        flag = false;
        //final ImageView iv = (ImageView) findViewById(R.id.imageView);
        //final Animation an = AnimationUtils.loadAnimation(getBaseContext(), R.anim.rotate);
        //final Animation an2 = AnimationUtils.loadAnimation(getBaseContext(), R.anim.fade_out);


        //String[] nomeCampos = new String[] {CriaBanco.getID(), CriaBanco.ge};
        //int[] idViews = new int[] {R.id.idLivro, R.id.nomeLivro};

        try {
            properties = getProperties("config.properties", this);
        }catch (Exception e){
            e.printStackTrace();
        }

        if(properties==null){
            Toast.makeText(this, "Erro ao ler properties.", Toast.LENGTH_SHORT).show();
            finish();
        }else {
            //iv.startAnimation(an);
            ipServidor = properties.getProperty("ipServidor");
            pathServicesServidor = properties.getProperty("pathServicesServidor");
            //broadcastReceiver = new WifiReceiver(this);
            //intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
            //registerReceiver(broadcastReceiver, intentFilter);
        }


        //an.setAnimationListener(new Animation.AnimationListener() {
          //  @Override
           // public void onAnimationStart(Animation animation) {

            //}

            //@Override
            //public void onAnimationEnd(Animation animation) {
             //   iv.startAnimation(an);
            //}

     //       @Override
      //      public void onAnimationRepeat(Animation animation) {

         //   }
        //});

        //BancoController crud = new BancoController(getBaseContext());
        //Cursor cursor = crud.carregaDados();
        UsuarioDAO dao = new UsuarioDAO(getBaseContext());
        List<UsuarioLite> all =  dao.recuperarTodos();
        String teste;

        if (all != null && all.size() > 0 ){
            String email = all.get(0).getEmail();//cursor.getString(cursor.getColumnIndex("email"));
            //String senha = cursor.getString(cursor.getColumnIndex("senha"));
            try {
                teste = new LoginAsyncTask(this).execute(this.ipServidor, this.pathServicesServidor, email).get();
                Gson gson = new Gson();
                Usuario obj = gson.fromJson(teste, Usuario.class);
                UsuarioLite usuarioLite = new UsuarioLite();

                usuarioLite.setNome(obj.getNome());
                usuarioLite.setEmail(obj.getEmail());
                usuarioLite.setCpf(obj.getCpf());
                usuarioLite.setSenha(obj.getSenha());

                //UsuarioDAO dao = new UsuarioDAO(getBaseContext());
                dao.salvar(usuarioLite);
                
                //// TODO: 06/10/2017 direcionar pela role do usuario para activity
                obj.getGroups();


            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }else {
            Intent i = new Intent(this, CadastrarUsuarioActivity.class);
            this.startActivity(i);
        }
    }


    public static Properties getProperties(String file, Context context) throws IOException {
        Properties properties = new Properties();;
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = assetManager.open(file);
        properties.load(inputStream);
        return properties;
    }

}