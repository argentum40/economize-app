package rvenancio2.br.com.economize_app.task;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.google.gson.Gson;

import rvenancio2.br.com.economize_app.activity.CadastrarUsuarioActivity;
import rvenancio2.br.com.economize_app.http.HttpClient;
import rvenancio2.br.com.economize_app.model.Usuario;

/**
 * Created by rvena on 04/10/2017.
 */

public class CadastraUsuarioAsyncTask extends AsyncTask<String, Void, String> {

    private ProgressDialog dialog;
    private CadastrarUsuarioActivity a;
    private String locations;


    public CadastraUsuarioAsyncTask(CadastrarUsuarioActivity ctx) {
        a = ctx;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = ProgressDialog.show(a, "Aguarde", "cadastrando");
    }

    @Override
    protected String doInBackground(String... params) {
        //economize.kinghost.net
        HttpClient client = new HttpClient(params[0]);
        String path;
        Usuario u = new Usuario();
        u.setNome(params[2]);
        u.setCpf(params[3]);
        u.setEmail(params[4]);
        u.setSenha(params[5]);
        //new CadastraUsuarioAsyncTask(activity).execute(ipServidor, pathServicesServidor,
         //       nomeString,cpfString,emailString,senha1String);

        try {
            Gson gson = new Gson();
            // converte objetos Java para JSON e retorna JSON como String
            String json = gson.toJson(u);
            ///economize/services/
            locations = client.sendPost(params[1]+"/usuario/cadastra", json);
            //TODO converter usuario para json e testar
            System.out.println(locations);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return locations;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        dialog.dismiss();
    }
}
