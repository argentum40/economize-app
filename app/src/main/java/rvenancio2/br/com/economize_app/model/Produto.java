package rvenancio2.br.com.economize_app.model;

import java.io.Serializable;
import java.util.List;



public class Produto implements Serializable {

	private Integer id;
	private String nome;
	private String codbarras;

	//private Categoria category;
	private Fabricante fabricante;

	private List<Empresa> empresas;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodbarras() {
		return codbarras;
	}
	public void setCodbarras(String codbarras) {
		this.codbarras = codbarras;
	}
	public Fabricante getFabricante() {
		return fabricante;
	}
	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}
	public List<Empresa> getEmpresa() {
		return empresas;
	}
	public void setEmpresa(List<Empresa> empresa) {
		this.empresas = empresa;
	}
	
	

}
