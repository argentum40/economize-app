package rvenancio2.br.com.economize_app.sqlite;

import android.content.ContentValues;
import android.content.Context;


/**
 * Created by rvena on 05/10/2017.
 */

public class UsuarioDAO  extends DAOBasico<UsuarioLite> {

    private static final String NOME_TABELA = "USUARIO";
    public static final String SCRIPT_CRIACAO_TABELA = "CREATE TABLE  USUARIO ( id INTEGER PRIMARY KEY " +
            "autoincrement, nome TEXT,cpf TEXT, email TEXT, senha TEXT)";
    public static final String SCRIPT_DELECAO_TABELA =  "DROP TABLE IF EXISTS " + NOME_TABELA;

    private static final String COLUNA_ID = "id";
    private static final String COLUNA_NOME = "nome";
    private static final String COLUNA_CPF = "cpf";
    private static final String COLUNA_EMAIL = "email";
    private static final String COLUNA_SENHA = "senha";


    public UsuarioDAO(Context context) {
        super(context);
    }

    @Override
    public String getNomeColunaPrimaryKey() {
        return COLUNA_ID;
    }

    @Override
    public String getNomeTabela() {
        return NOME_TABELA;
    }

    public ContentValues entidadeParacontentValues(UsuarioLite usuario) {
        ContentValues contentValues = new ContentValues();
        if(usuario.getId() > 0) {
            contentValues.put(COLUNA_ID, usuario.getId());
        }
//        contentValues.put(COLUNA_IDADE, pessoa.getIdade());
        contentValues.put(COLUNA_NOME, usuario.getNome());
        contentValues.put(COLUNA_CPF, usuario.getCpf());
        contentValues.put(COLUNA_EMAIL, usuario.getEmail());
        contentValues.put(COLUNA_SENHA, usuario.getSenha());
        return contentValues;
    }

    @Override
    public UsuarioLite contentValuesParaEntidade(ContentValues contentValues) {
        UsuarioLite pessoa = new UsuarioLite();
        pessoa.setId(contentValues.getAsInteger(COLUNA_ID));
        pessoa.setCpf(contentValues.getAsString(COLUNA_CPF));
        pessoa.setEmail(contentValues.getAsString(COLUNA_EMAIL));
        pessoa.setSenha(contentValues.getAsString(COLUNA_SENHA));
        pessoa.setNome(contentValues.getAsString(COLUNA_NOME));
        return pessoa;
    }

}
