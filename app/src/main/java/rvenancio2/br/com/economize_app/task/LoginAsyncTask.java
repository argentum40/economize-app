package rvenancio2.br.com.economize_app.task;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import rvenancio2.br.com.economize_app.activity.SplashActivity;
import rvenancio2.br.com.economize_app.http.HttpClient;

/**
 * Created by rvena on 04/10/2017.
 */

public class LoginAsyncTask extends AsyncTask<String, Void, String> {

    private ProgressDialog dialog;
    private SplashActivity a;
    private String locations;


    public LoginAsyncTask(SplashActivity ctx) {
        a = ctx;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = ProgressDialog.show(a, "Aguarde", "logando");
    }

    @Override
    protected String doInBackground(String... params) {
        //economize.kinghost.net
        HttpClient client = new HttpClient(params[0]);
        String path;
        try {
            ///economize/services /@email
            //TODO este serviço ainda nao esta pronto
            //Gson gson = new Gson();

            //String json = gson.toJson(params[2]);

            locations = client.sendGETJson(params[1]+"/usuario/recuperapeloemail/"+ params[2]);
            //client.send
            System.out.println(locations);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return locations;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        dialog.dismiss();
    }
}
