package rvenancio2.br.com.economize_app.http;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;


//http://economize.kinghost.net/economize/services/empresa/1
public class HttpClient {

	public final static Logger logger = Logger.getLogger(HttpClient.class.toString());

	/**
	 * Versão do protocolo utilizada
	 */
	public final static String HTTP_VERSION = "HTTP/1.1";

	private String host;
	private int port;
	private int status;
	private HttpURLConnection request;

	public HttpClient(String host, int port) {
		super();
		this.host = host;
		this.port = port;
	}

	public HttpClient(String host) {
		super();
		this.host = host;
		//this.port = port;
	}

	/*
	 * public String getURIRawContent(String path) throws UnknownHostException,
	 * IOException { Socket socket = null; try { // Abre a conexão socket = new
	 * Socket(this.host, this.port); PrintWriter out = new
	 * PrintWriter(socket.getOutputStream(), true); BufferedReader in = new
	 * BufferedReader(new InputStreamReader(socket.getInputStream()));
	 * 
	 * // Envia a requisição out.println("GET " + path + " " + HTTP_VERSION);
	 * out.println("Host: " + this.host); out.println("Connection: Close");
	 * out.println();
	 * 
	 * boolean loop = true; StringBuffer sb = new StringBuffer();
	 * 
	 * // recupera a resposta quando ela estiver disponível while (loop) { if
	 * (in.ready()) { int i = 0; while ((i = in.read()) != -1) {
	 * sb.append((char) i); } loop = false; } }
	 * 
	 * response = sb.toString();
	 * 
	 * String[] lines = sb.toString().split("\\n"); for (String s : lines) {
	 * System.out.println("Content = " + s); } String[] s = lines[0].split(" ");
	 * 
	 * status = Integer.parseInt(s[1]); content = lines[8];
	 * 
	 * return lines[8];
	 * 
	 * } catch (Exception e) { return e.getMessage(); } finally { if (socket !=
	 * null) { socket.close(); } } } //
	 */

	public int getStatusCode() {
		return status;
	}

	public static void main(String[] args) {
		HttpClient client = new HttpClient("192.168.1.139", 8280);
		// http://192.168.1.139:8280/servertccwebservice/serverTCC/rotas/banheiro/quarto1
			try {
				String s = "";
				client.sendPost("/servertccwebservice/serverTCC/cadastramappoint", s);
				// client.sendJson("http://192.168.1.139:8280/servertccwebservice/serverTCC/cadastramappoint/crunchifyService",s);
			} catch (Exception e) {
					// TODO: handle exception
					System.out.println(e.getMessage());
			}

	}



//ok
	public String sendPost(String url, String json) throws MinhaException {

		try {
			// Cria um objeto HttpURLConnection:
			String urlTemp = "http://" + host + url;
			request = (HttpURLConnection) new URL(urlTemp).openConnection();

			try {
				// Define que a conexão pode enviar informações e obtê-las de
				// volta:
				request.setDoOutput(true);
				request.setDoInput(true);

				// Define o content-type:
				request.setRequestProperty("Content-Type", "application/json");

				// Define o método da requisição:
				request.setRequestMethod("POST");

				// Conecta na URL:
				request.connect();

				try{
					// Escreve o objeto JSON usando o OutputStream da requisição:
					OutputStream outputStream = request.getOutputStream();
					outputStream.write(json.getBytes("UTF-8"));
				}catch (Exception e){
					e.printStackTrace();
				}

				status = request.getResponseCode();

				return readResponse(request);
			} finally {
				request.disconnect();
			}
		} catch (IOException ex) {
			throw new MinhaException(ex);
		}
	}

	public String sendGET(String url) throws MinhaException {

		try {
			// Cria um objeto HttpURLConnection:
			String urlTemp = "http://" + host + url;
			request = (HttpURLConnection) new URL(urlTemp).openConnection();

			try {
				request.setRequestMethod("GET");
				request.setRequestProperty("Content-Type", "text/plain");
				status = request.getResponseCode();
				request.connect();

				return readResponse(request);
			} finally {
				request.disconnect();
			}
		} catch (IOException ex) {
			throw new MinhaException(ex);
		}
	}

	public void sendJson(String serv, String json) {
		// Step2: Now pass JSON File Data to REST Service
		try {
			URL url = new URL(serv);
			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(json);
			out.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			while (in.readLine() != null) {
			}
			System.out.println("\nCrunchify REST Service Invoked Successfully..");
			in.close();
		} catch (Exception e) {
			System.out.println("\nError while calling Crunchify REST Service");
			System.out.println(e);
		}
	}

	public String sendGETJson(String url) throws MinhaException {

		try {
			String urlTemp = "http://" + host + url;
			request = (HttpURLConnection) new URL(urlTemp).openConnection();

			try {
				request.setRequestMethod("GET");
				request.setRequestProperty("Content-Type", MediaType.APPLICATION_JSON);
				status = request.getResponseCode();
				request.connect();

				/*
				BufferedReader in = new BufferedReader(
						new InputStreamReader(request.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				//print result
				System.out.println(response.toString());
				*/

				return readResponse(request);
			} finally {
				request.disconnect();
			}
		} catch (IOException ex) {
			throw new MinhaException(ex);
		}
	}

	private String readResponse(HttpURLConnection request) throws IOException {
		ByteArrayOutputStream os = null;
			try {

				InputStream is = request.getInputStream();
				os = new ByteArrayOutputStream();
				int b;

				while ((b = is.read()) != -1) {
					os.write(b);
			}

		}catch (Exception e){
			e.printStackTrace();
		}
		return new String(os.toByteArray());
	}

	public static class MinhaException extends Exception {
		private static final long serialVersionUID = 1L;

		public MinhaException(Throwable cause) {
			super(cause);
		}
	}

}
